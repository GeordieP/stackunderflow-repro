# Stackunderflow

Demo app for the scenario where an ecto schema `has_many` of a type, and we need a field that represents zero or one of the associated structures. In this repo, a `%Question{}` has many `%Answers{}`, but one `accepted_answer` (id), which defaults to nil but can be set by a user.

Steps:

1. Create a new question
2. Add a new answer to the question, on question's :show page

Expected:

- New answer only shows in the answers list section

Actual:

- New answer shows in the answers list section AND the accepted answer section

---

3. Add a second answer to the question
4. Click "Accept answer" button on this new answer

Expected:

- Accepted answer field is updated to display the chosen answer

Actual:

- Accepted answer field is updated to display the chosen answer
- Both answers in the answer list are now the same value
