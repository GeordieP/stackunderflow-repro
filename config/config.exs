# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :stackunderflow,
  ecto_repos: [Stackunderflow.Repo]

# Configures the endpoint
config :stackunderflow, StackunderflowWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "AMjo1F/WTQwwRSDl4uhyDB6GZXU3q4fHfRn/p4RXzipQ3i/YbZ4RUURD/Y7nC8/P",
  render_errors: [view: StackunderflowWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Stackunderflow.PubSub,
  live_view: [signing_salt: "K7/o7jNe"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
