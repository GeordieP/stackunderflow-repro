defmodule StackunderflowWeb.AnswerController do
  use StackunderflowWeb, :controller

  alias Stackunderflow.Questions
  alias Stackunderflow.Questions.{Question, Answer}

  plug :scrub_params, "answer" when action in [:create, :update]

  def create(conn, %{"answer" => answer_params, "question_id" => question_id}) do
    question = Questions.get_question!(question_id)

    changeset =
      question
      |> Ecto.build_assoc(:answers)
      |> Questions.change_answer(answer_params)

    case Stackunderflow.Repo.insert(changeset) do
      {:ok, _answer} ->
        conn
        |> put_flash(:info, "Answer saved successfully")
        |> redirect(to: Routes.question_path(conn, :show, question))

      {:error, changeset} ->
        render(conn, StackunderflowWeb.QuestionView, "show.html",
          question: question,
          answer_changeset: changeset
        )
    end
  end

  def accept_answer(conn, %{"id" => answer_id, "question_id" => question_id}) do
    with %Question{} = question <- Questions.get_question!(question_id),
         %Answer{} = answer <- Questions.get_answer!(answer_id) do
      Questions.update_question(question, %{accepted_answer: Map.from_struct(answer)})

      conn
      |> put_flash(:info, "Answer accepted")
      |> redirect(to: Routes.question_path(conn, :show, question))
    else
      _ ->
        conn
        |> put_flash(:error, "Could not set answer as accepted")
        |> redirect(to: Routes.question_path(conn, :show, question_id))
    end
  end
end
