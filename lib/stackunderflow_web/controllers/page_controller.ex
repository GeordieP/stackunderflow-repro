defmodule StackunderflowWeb.PageController do
  use StackunderflowWeb, :controller

  def index(conn, _params) do
    redirect(conn, to: Routes.question_path(conn, :index))
  end
end
