defmodule Stackunderflow.Questions.Question do
  use Ecto.Schema
  import Ecto.Changeset

  schema "questions" do
    field :body, :string
    field :title, :string
    has_many :answers, Stackunderflow.Questions.Answer
    has_one :accepted_answer, Stackunderflow.Questions.Answer, on_replace: :nilify

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:title, :body])
    |> cast_assoc(:accepted_answer)
    |> validate_required([:title, :body])
  end
end
