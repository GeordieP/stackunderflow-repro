defmodule Stackunderflow.Repo do
  use Ecto.Repo,
    otp_app: :stackunderflow,
    adapter: Ecto.Adapters.Postgres
end
